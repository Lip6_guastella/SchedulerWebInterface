# Scheduler Web Interaface
### Français
Ce projet est l'interface graphique pour l'algorithme de ordonnancement de plans des taches. Ceux-ci sont le logiciels utilisés:

- Netbeans 8.2
- Java 7
- Apache commons
- Primefaces 6.0
- Wildfly 10.1.0

Le projet est de type Maven.

### English
This project is the graphical interface for the plans of tasks scheduling algorithm. These are the requirements:

- Netbeans 8.2
- Java 7
- Apache commons
- Primefaces 6.0
- Wildfly 10.1.0

The project is of type Maven.
