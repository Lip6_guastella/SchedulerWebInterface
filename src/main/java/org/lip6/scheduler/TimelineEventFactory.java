package org.lip6.scheduler;

import org.lip6.scheduler.webInterface.TaskTimelineEvent;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.lip6.scheduler.utils.DateUtils;
import org.primefaces.model.timeline.TimelineEvent;

/**
 * Static factory for the TaskTimelineEvent class
 *
 * @author davide
 */
public class TimelineEventFactory {

    /**
     * Create a new TaskTimelineEvent instance, that is, a graphical
     * representation of a task in the Timeline primefaces control.
     *
     * @param plans
     * @param task
     * @param startingTime
     * @param resourceID
     * @param Ws
     * @return
     */
    public static TaskTimelineEvent fromTask(final List<Plan> plans, final Task task, int startingTime, int resourceID, Date Ws) {
        int processingTime = task.getProcessingTime(startingTime);

        if (task.getID() == 1 && task.getPlanID() == 1) {
            System.out.println("P(" + Integer.toString(startingTime) + ") for " + task + ": " + Integer.toString(processingTime));
        }
        Date start = DateUtils.sumSecondsToDate(Ws, startingTime);
        Date end = DateUtils.sumSecondsToDate(start, processingTime);

        String resString = "<html><body><center><p><b>&rho;<sub style='position: relative; bottom: -.3em;'>"
                + Integer.toString(resourceID) + "</sub></b></p></center></body></html>";

        TimelineEvent ev = new TimelineEvent(task.toHTMLString(false), start, end, false, resString, "plan" + task.getPlanID());

        Optional<Plan> p = plans.stream().filter(x -> x.getID() == task.getPlanID()).findFirst();
        if (!p.isPresent()) {
            throw new IllegalArgumentException("Cannot find corresponding plan for task " + task);
        }

        TaskTimelineEvent tt = new TaskTimelineEvent(p.get(), task, startingTime, ev);
        return tt;
    }
}
