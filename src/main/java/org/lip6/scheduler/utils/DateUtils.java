package org.lip6.scheduler.utils;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author davide
 */
public class DateUtils {

    public static java.util.Date sumSecondsToDate(java.util.Date date, int secs) {
        long secsToAddInMs = secs * 1000;
        return new java.util.Date(date.getTime() + secsToAddInMs);
    }

    public static java.util.Date sumMillisecondsToDate(java.util.Date date, int milliSecs) {
        return new java.util.Date(date.getTime() + milliSecs);
    }

    public static java.util.Date subtractMillisecondsToDate(java.util.Date date, int milliSecs) {
        return new java.util.Date(date.getTime() - milliSecs);
    }

    public static Date yesterday() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    public static Date tomorrow() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        return cal.getTime();
    }

}
