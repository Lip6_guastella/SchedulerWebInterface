package org.lip6.scheduler;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lip6.scheduler.utils.CSVParser;

/**
 *
 * @author davide
 */
public class PlanFactory {

    public static Set<Plan> getPlansFromFile(String filename) {
        Map<Integer, Plan> p = new HashMap<>();

        try {
            p = CSVParser.parse(filename);
        } catch (IOException | ParseException ex) {
            Logger.getLogger(PlanFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new HashSet<>(p.values());
    }

}
