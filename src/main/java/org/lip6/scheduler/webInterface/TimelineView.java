package org.lip6.scheduler.webInterface;

import java.awt.event.ActionEvent;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import org.lip6.scheduler.PlanFactory;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import org.lip6.scheduler.Plan;
import org.lip6.scheduler.Schedule;
import org.lip6.scheduler.Task;
import org.lip6.scheduler.TaskSchedule;
import org.lip6.scheduler.TimelineEventFactory;
import org.lip6.scheduler.algorithm.Scheduler;
import org.lip6.scheduler.algorithm.SchedulerFactory;
import org.lip6.scheduler.utils.CSVParser;

import org.lip6.scheduler.utils.DateUtils;
import org.primefaces.component.timeline.Timeline;
import org.primefaces.component.timeline.TimelineRenderer;
import org.primefaces.component.timeline.TimelineUpdater;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.timeline.TimelineSelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;
import org.primefaces.model.timeline.TimelineEvent;
import org.primefaces.model.timeline.TimelineModel;

@ManagedBean(name = "timelineView")
@ViewScoped
public class TimelineView implements Serializable {

    private static final int WE_MAX = 100000;

    Path uploadedFile;
    Path uploadedProcTimeFuncFile;

    private List<TaskSchedule> taskSchedule;
    private List<TaskTimelineEvent> events;
    private Set<Plan> discardedPlans;
    private List<Plan> allPlans;
    private Plan selectedPlan;
    private Plan selectedDiscardedPlan;

    private TaskTimelineEvent selectedEvent;

    private TimelineModel model;
    private int Ws;
    private int We;
    private int maxResourceCapacity;

    private boolean autoFixWe;
    private boolean selectable;
    private boolean zoomable;
    private boolean moveable;
    private boolean stackEvents;
    private String eventStyle;
    private boolean axisOnTop;
    private boolean showCurrentTime;
    private boolean showNavigation;

    private UploadedFile selectedFile;
    private UploadedFile selectedProcessingTimeFuncFile;

    private InputStream selectedFileStream;
    private InputStream selectedProcTimeFunFileStream;

    private String uploadedFileName;
    private String uploadedProcessingTimeFunctionFileName;

    private LineChartModel processingTimeFuncModel;

    public TimelineView() {
        uploadedFile = Paths.get("");
        uploadedProcTimeFuncFile = Paths.get("");
        selectable = true;
        zoomable = true;
        moveable = true;
        stackEvents = false;
        eventStyle = "box";
        axisOnTop = false;
        showCurrentTime = false;
        showNavigation = false;
        selectedEvent = null;
        Ws = 1;
        We = 180;
        maxResourceCapacity = 1;
        events = new ArrayList<>();
        model = new TimelineModel();
        discardedPlans = new HashSet<>();
        allPlans = new ArrayList<>();
        taskSchedule = new ArrayList<>();
        selectedFile = null;
        autoFixWe = false;

        processingTimeFuncModel = new LineChartModel();
    }

    public LineChartModel getProcessingTimeFuncModel() {
        createLineModel();
        processingTimeFuncModel.setLegendPosition("e");

        return processingTimeFuncModel;
    }

    public void setProcessingTimeFuncModel(LineChartModel processingTimeFuncModel) {
        this.processingTimeFuncModel = processingTimeFuncModel;
    }

    private void createLineModel() {
        processingTimeFuncModel = new LineChartModel();
        LineChartSeries series1 = new LineChartSeries();
        series1.setLabel("p(t)");
        series1.setSmoothLine(true);

        if (selectedEvent != null) {

            Task task = selectedEvent.task;
            processingTimeFuncModel.setTitle("Processing time function for " + task);

            for (Integer t : task.deltaValues().keySet()) {
                series1.set(t, task.getProcessingTime(t));
            }

            Axis xAxis = processingTimeFuncModel.getAxis(AxisType.X);
            xAxis.setMin(selectedEvent.task.getReleaseTime());
            xAxis.setMax(selectedEvent.task.getDueDate());
            xAxis.setLabel("t");

            int ymax = task.deltaValues().keySet().stream().mapToInt(t -> task.getProcessingTime(t)).max().getAsInt() + 2;
            int ymin = task.deltaValues().keySet().stream().mapToInt(t -> task.getProcessingTime(t)).min().getAsInt() - 2;
            Axis yAxis = processingTimeFuncModel.getAxis(AxisType.Y);
            yAxis.setMin(ymin);
            yAxis.setMax(ymax);
            yAxis.setLabel("p(t)");

        }
        processingTimeFuncModel.addSeries(series1);
    }

    public void onShowProcessingTimeFunctionPlot() {
        if (selectedEvent == null) {
            addMessage(FacesMessage.SEVERITY_WARN, "Warning", "No task selected");
            return;
        }

        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.execute("PF('ProcessingTimeFunctionDialog').show()");
    }
    
    
    public void unloadProcessingTimeFunctions(){
        uploadedProcTimeFuncFile = Paths.get("");
        setUploadedProcessingTimeFunctionFileName("");
    }

    public boolean isAutoFixWe() {
        return autoFixWe;
    }

    public void setAutoFixWe(boolean autoFixWe) {
        this.autoFixWe = autoFixWe;
    }

    public String getUploadedFileName() {
        return uploadedFileName;
    }

    public void setUploadedFileName(String uploadedFileName) {
        this.uploadedFileName = uploadedFileName;
    }

    public String getUploadedProcessingTimeFunctionFileName() {
        return uploadedProcessingTimeFunctionFileName;
    }

    public void setUploadedProcessingTimeFunctionFileName(String uploadedProcessingTimeFunctionFileName) {
        this.uploadedProcessingTimeFunctionFileName = uploadedProcessingTimeFunctionFileName;
    }

    public String getCSSClassForPlan(int id) {
        return "div.plan" + Integer.toString(id);
    }

    public Plan getSelectedPlan() {
        return selectedPlan;
    }

    public void setSelectedPlan(Plan selectedPlan) {
        this.selectedPlan = selectedPlan;
    }

    public List<Plan> getAllPlans() {
        return allPlans;
    }

    public void setAllPlans(List<Plan> allPlans) {
        this.allPlans = allPlans;
    }

    public Set<Plan> getDiscardedPlans() {
        return discardedPlans;
    }

    public void setDiscardedPlans(Set<Plan> discardedPlans) {
        this.discardedPlans = discardedPlans;
    }

    public Plan getSelectedDiscardedPlan() {
        return selectedDiscardedPlan;
    }

    public void setSelectedDiscardedPlan(Plan selectedDiscardedPlan) {
        this.selectedDiscardedPlan = selectedDiscardedPlan;
    }

    public TimelineModel getModel() {
        return model;
    }

    public InputStream getSelectedFileStream() {
        return selectedFileStream;
    }

    public void setSelectedFileStream(InputStream selectedFileStream) {
        this.selectedFileStream = selectedFileStream;
    }

    public InputStream getSelectedProcTimeFunFileStream() {
        return selectedProcTimeFunFileStream;
    }

    public void setSelectedProcTimeFunFileStream(InputStream selectedProcTimeFunFileStream) {
        this.selectedProcTimeFunFileStream = selectedProcTimeFunFileStream;
    }
    
    

    public int getWs() {
        return Ws;
    }

    public void setWs(int Ws) {
        this.Ws = Ws;

    }

    public int getWe() {
        return We;
    }

    public void setWe(int We) {
        this.We = We;
    }

    public int getMaxResourceCapacity() {
        return maxResourceCapacity;
    }

    public void setMaxResourceCapacity(int maxResourceCapacity) {
        this.maxResourceCapacity = maxResourceCapacity;
        stackEvents = maxResourceCapacity > 1;
    }

    public UploadedFile getSelectedFile() {
        return selectedFile;
    }

    public void setSelectedFile(UploadedFile selectedFile) {
        this.selectedFile = selectedFile;
    }

    public UploadedFile getSelectedProcessingTimeFuncFile() {
        return selectedProcessingTimeFuncFile;
    }

    public void setSelectedProcessingTimeFuncFile(UploadedFile selectedProcessingTimeFuncFile) {
        this.selectedProcessingTimeFuncFile = selectedProcessingTimeFuncFile;
    }

    public TaskTimelineEvent getSelectedEvent() {
        return selectedEvent;
    }

    public void setSelectedEvent(TaskTimelineEvent selectedEvent) {
        this.selectedEvent = selectedEvent;
    }

    public boolean isSelectable() {
        return selectable;
    }

    public void setSelectable(boolean selectable) {
        this.selectable = selectable;
    }

    public boolean isZoomable() {
        return zoomable;
    }

    public void setZoomable(boolean zoomable) {
        this.zoomable = zoomable;
    }

    public boolean isMoveable() {
        return moveable;
    }

    public void setMoveable(boolean moveable) {
        this.moveable = moveable;
    }

    public boolean isStackEvents() {
        return stackEvents;
    }

    public void setStackEvents(boolean stackEvents) {
        this.stackEvents = stackEvents;
    }

    public String getEventStyle() {
        return eventStyle;
    }

    public void setEventStyle(String eventStyle) {
        this.eventStyle = eventStyle;
    }

    public boolean isAxisOnTop() {
        return axisOnTop;
    }

    public void setAxisOnTop(boolean axisOnTop) {
        this.axisOnTop = axisOnTop;
    }

    public boolean isShowCurrentTime() {
        return showCurrentTime;
    }

    public void setShowCurrentTime(boolean showCurrentTime) {
        this.showCurrentTime = showCurrentTime;
    }

    public boolean isShowNavigation() {
        return showNavigation;
    }

    public void setShowNavigation(boolean showNavigation) {
        this.showNavigation = showNavigation;
    }

    public List<TaskSchedule> getTaskSchedule() {
        return taskSchedule;
    }

    public void setTaskSchedule(List<TaskSchedule> taskSchedule) {
        this.taskSchedule = taskSchedule;
    }

    /**
     * Callback method invoked when a timeline event (a task) is selected.
     *
     * @param e
     */
    public void onTaskSelected(TimelineSelectEvent e) {
        Optional<TaskTimelineEvent> event = events.stream().filter(ev -> ev.getEvent().equals(e.getTimelineEvent())).findFirst();
        if (event.isPresent()) {
            selectedEvent = event.get();
            System.err.println("Selected task: " + selectedEvent.getTask());
        }
    }

    public void handleProcTimeFuncFileUpload(FileUploadEvent event) {
        selectedProcessingTimeFuncFile = event.getFile();
        uploadedProcessingTimeFunctionFileName = selectedProcessingTimeFuncFile.getFileName();
        addMessage("Success", "File " + selectedProcessingTimeFuncFile.getFileName() + " has been uploaded.");
        try {
            setSelectedProcTimeFunFileStream(selectedProcessingTimeFuncFile.getInputstream());

            uploadedProcTimeFuncFile = Files.createTempFile(".tmp", "procTimeFunc.csv");
            Files.copy(selectedProcessingTimeFuncFile.getInputstream(), uploadedProcTimeFuncFile, StandardCopyOption.REPLACE_EXISTING);

        } catch (IOException ex) {
            Logger.getLogger(TimelineView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Upload a CSV file
     *
     * @param event
     */
    public void handleFileUpload(FileUploadEvent event) {

        selectedFile = event.getFile();
        uploadedFileName = selectedFile.getFileName();
        addMessage("Success", "File " + selectedFile.getFileName() + " has been uploaded.");
        try {
            setSelectedFileStream(selectedFile.getInputstream());

            uploadedFile = Files.createTempFile(".tmp", "planSet.csv");
            Files.copy(selectedFile.getInputstream(), uploadedFile, StandardCopyOption.REPLACE_EXISTING);

        } catch (IOException ex) {
            Logger.getLogger(TimelineView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Invoked when "Schedule" button is clicked.
     */
    public void onScheduleButtonClicked() {
        //Check that the time window is valid
        if (Ws >= We) {
            addMessage(FacesMessage.SEVERITY_ERROR, "Error", "Wrong temporal windows boundaries.");
            return;
        }

        //Check that a CSV file containing the plans has been uploaded
        if (Files.notExists(uploadedFile)) {
            addMessage(FacesMessage.SEVERITY_ERROR, "Scheduler", "No plans uploaded.");
            return;
        }

        //Get the updater object, needed to render the timeline
        TimelineUpdater updater = TimelineUpdater.getCurrentInstance(":form:timeline");

        //Clear the timeline model
        model.clear(updater);

        // create a new timeline model  
        model = new TimelineModel();
        events.clear();

        Scheduler scheduler = null;

        int we = We;
        if (autoFixWe) {
            we = WE_MAX;
        }

        try {
            scheduler = SchedulerFactory.get(maxResourceCapacity, Ws, we, Files.newInputStream(uploadedFile));
        } catch (IOException ex) {
            Logger.getLogger(TimelineView.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (scheduler == null) {
            addMessage(FacesMessage.SEVERITY_ERROR, "Scheduler", "An error occurred.");
            return;
        }

        if (!Files.notExists(uploadedProcTimeFuncFile)) {
            try {
                CSVParser.parseDeltaValues(Files.newInputStream(uploadedProcTimeFuncFile), scheduler.getPlans());
                addMessage("Info", "Processing time functions set.");
            } catch (ParseException | IOException ex) {
                Logger.getLogger(TimelineView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        //-----------------------------------------------
        //Set the scheduler properties
        scheduler.setCalculateOptimalWe(autoFixWe);

        //Schedule the plans
        long lStartTime = System.nanoTime();
        Schedule schedule = scheduler.buildSchedule();
        long lEndTime = System.nanoTime();
        if (schedule == null) {
            addMessage(FacesMessage.SEVERITY_ERROR, "Scheduler", "An error occurred. Check the log.");
            return;
        }

        allPlans.addAll(scheduler.getPlans());
        discardedPlans.addAll(scheduler.getUnscheduledPlans());

        System.out.println("Elapsed time in milliseconds: " + Long.toString(TimeUnit.MILLISECONDS.convert(lEndTime - lStartTime, TimeUnit.NANOSECONDS)));
        taskSchedule.clear();
        taskSchedule.addAll(schedule.taskSchedules());

        setupGanttDiagram(scheduler, schedule);

        System.out.println("SCHEDULED: ");
        System.out.println(schedule.taskSchedules().stream().map(x -> Integer.toString(x.getTask().getPlanID())).distinct().collect(Collectors.joining(",")));
    }

    /**
     * Given a scheduler and a solution, setup the gantt diagram
     *
     * @param scheduler
     * @param solution
     */
    private void setupGanttDiagram(Scheduler scheduler, Schedule solution) {
        //Get the updater object, needed to render the timeline
        TimelineUpdater updater = TimelineUpdater.getCurrentInstance(":form:timeline");

        Date wsDate = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(wsDate);
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE), cal.get(Calendar.HOUR_OF_DAY), 0, 0);

        wsDate = cal.getTime();

        Date start = DateUtils.sumSecondsToDate(wsDate, Ws);
        if (autoFixWe) {
            We = scheduler.getEndEvent().getTimeInstant();
        }
        Date end = DateUtils.sumSecondsToDate(wsDate, We);
        addMessage("Scheduler", "Scheduling from " + Integer.toString(Ws) + " to " + Integer.toString(We));

        for (TaskSchedule ts : solution.taskSchedules()) {
            for (Integer resource : ts.getTask().getResourcesID()) {
                TaskTimelineEvent ev = TimelineEventFactory.fromTask(scheduler.getPlans(), ts.getTask(), ts.getStartingTime(), resource, wsDate);
                events.add(ev);
                model.add(ev.getEvent(), updater);
            }
        }

        //Add the Ws/We events
        model.add(new TimelineEvent("<img src='https://image.flaticon.com/icons/svg/33/33622.svg' style='width:30px;height:30px;'>",
                start, false, "<html><body><center><p><b>[W<sub>s</sub>,W<sub>e</sub>]</b></p></center></body></html>", "ws"), updater);
        model.add(new TimelineEvent("<img src='https://image.flaticon.com/icons/svg/33/33622.svg' style='width:30px;height:30px;'>",
                end, false, "<html><body><center><p><b>[W<sub>s</sub>,W<sub>e</sub>]</b></p></center></body></html>", "we"), updater);
    }

    public DefaultDiagramModel getTaskHiearchy() {
        DefaultDiagramModel diagramModel = new DefaultDiagramModel();
        diagramModel.setMaxConnections(-1);

        if (selectedEvent == null) {
            return diagramModel;
        }
        Plan plan = selectedEvent.getPlan();

        Map<Integer, String> elementsID = new HashMap<>();

        int yIncrement = 10;
        for (Task task : plan.getTasks()) {
            Element e = new Element(task.getID(), "5em", Integer.toString(yIncrement) + "em");
            e.addEndPoint(new DotEndPoint(EndPointAnchor.BOTTOM));
            diagramModel.addElement(e);
            elementsID.put(task.getID(), e.getId());
            yIncrement += 12;
        }

        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:3}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");

        for (Task task : plan.getTasks()) {
            for (Integer successor : task.getSuccessors()) {

                Element a = diagramModel.findElement(elementsID.get(task.getID()));
                Element b = diagramModel.findElement(elementsID.get(successor));
                diagramModel.connect(new Connection(a.getEndPoints().get(0), b.getEndPoints().get(0), connector));
            }
        }
        return diagramModel;
    }

    public int getEmployedTimeUnits(Plan p) {
        return p.getExecutionTime();
    }

    public int getNumberOfTasks(Plan p) {
        return p.getTasks().size();
    }

    public String getStyleClass(int planID) {
        return "plan" + Integer.toString(planID);
    }

    public void onClearButtonClicked() {
        model.clear(TimelineUpdater.getCurrentInstance(":form:timeline"));
        selectedEvent = null;
        selectedPlan = null;
        addMessage("Scheduler", "Scheduling cleared.");
    }

    public void onDeleteSelectedTask() {
        if (selectedEvent == null) {
            addMessage(FacesMessage.SEVERITY_WARN, "Error", "No task selected.");
            return;
        }

        addMessage("Info", "Removing plan " + Integer.toString(selectedEvent.getPlan().getID()));
        selectedDiscardedPlan = selectedEvent.getPlan();
        discardedPlans.add(selectedEvent.getPlan());
    }

    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void addMessage(FacesMessage.Severity severity, String summary, String detail) {
        FacesMessage message = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

}
