package org.lip6.scheduler.webInterface;

import org.lip6.scheduler.Plan;
import org.lip6.scheduler.Task;
import org.primefaces.model.timeline.TimelineEvent;

/**
 *
 * @author davide
 */
public class TaskTimelineEvent extends TimelineEvent {

    private final Plan plan;
    private final TimelineEvent event;
    public Task task;
    private final int startingTime;
    private final int completionTime;

    public TaskTimelineEvent(Plan plan, Task task, int startingTime, TimelineEvent event) {
        this.plan = plan;
        this.event = event;
        this.task = task;
        this.startingTime = startingTime;
        this.completionTime = startingTime + task.getProcessingTime();
    }

    public int getCompletionTime() {
        return startingTime + task.getProcessingTime();
        //return completionTime;
    }

    public Plan getPlan() {
        return plan;
    }
    
    public String getPlanName()
    {
        return plan.getName();
    }

    public TimelineEvent getEvent() {
        return event;
    }

    public Task getTask() {
        return task;
    }

    public int getStartingTime() {
        return startingTime;
    }

    public int getDueDate() {
        return startingTime + task.getProcessingTime();
    }

}
